package com.itheima.code.build;

import java.util.Map;

/****
 * @Author:shenkunlin
 * @Description:Pojo构建
 * @Date 2019/6/14 19:13
 *****/
public class VoBuilder {
    /***
     * 构建Pojo
     * @param dataModel
     */
    public static void builder(Map<String, Object> dataModel) {
        // 生成Pojo层文件
        BuilderFactory.builder(dataModel,
                "/template/vo",
                "Vo.java",
                TemplateBuilder.PACKAGE_VO,
                "VO.java");
    }

}
