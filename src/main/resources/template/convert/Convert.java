package ${package_convert};

import ${package_pojo}.${Table};
import ${package_dto}.${Table}DTO;
import ${package_vo}.${Table}VO;

/**
 * ${Table}Convert
 *
 * @author ${author}
 * @since ${.now?string("yyyy/MM/dd HH:mm")}
 */
@Mapper(componentModel = "spring")
public class ${Table}Convert extends Dto2PoBaseConvert<${Table}DTO, ${Table}>, Vo2PoBaseConvert<${Table}VO, ${Table}>  {

}
