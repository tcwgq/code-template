package ${package_mapper};

import ${package_pojo}.${Table};
import com.msun.core.db.mapper.MsunBaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * ${Table}Dao
 *
 * @author ${author}
 * @since ${.now?string("yyyy/MM/dd HH:mm")}
 */
@Mapper
public interface ${Table}Mapper extends MsunBaseMapper<${Table}> {

}
