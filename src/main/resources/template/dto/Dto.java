package ${package_dto};

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.*;
import java.io.Serializable;
<#list typeSet as set>
import ${set};
</#list>

/**
 * ${Table}DTO
 *
 * @author ${author}
 * @since ${.now?string("yyyy/MM/dd HH:mm")}
 */
@ApiModel(description = "${Table}DTO", value = "${Table}DTO")
@Data
public class ${Table}DTO extends BaseDTO {
<#list models as model>
	/**
	 * ${model.desc!""}
	 */
	@ApiModelProperty(value = "${model.desc!""}", required = false)
	private ${model.simpleType} ${model.name};

</#list>
}
