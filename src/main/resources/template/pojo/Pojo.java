package ${package_pojo};

<#if swagger==true>
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
import javax.persistence.*;
import java.io.Serializable;
<#list typeSet as set>
import ${set};
</#list>

/**
 * ${Table}Bean
 *
 * @author ${author}
 * @since ${.now?string("yyyy/MM/dd HH:mm")}
 */
@Data
@TableName("${TableName}")
public class ${Table} extends BasePO {
<#list models as model>
	/**
	 * ${model.desc!""}
	 */
	<#if model.id==true>
		<#if model.identity=='YES'>
	@TableId(type = IdType.AUTO)
		<#else>
	@TableId(type = IdType.INPUT)
		</#if>
	<#else>
	@TableField(value = "${model.column}")
	</#if>
	private ${model.simpleType} ${model.name};

</#list>
}
