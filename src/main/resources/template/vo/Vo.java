package ${package_vo};

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.*;
import java.io.Serializable;
<#list typeSet as set>
import ${set};
</#list>

/**
 * ${Table}VO
 *
 * @author ${author}
 * @since ${.now?string("yyyy/MM/dd HH:mm")}
 */
@ApiModel(description = "${Table}VO", value = "${Table}VO")
@Data
public class ${Table}VO extends BaseVO {
<#list models as model>
	/**
	 * ${model.desc!""}
	 */
	@ApiModelProperty(value = "${model.desc!""}", required = false)
	private ${model.simpleType} ${model.name};

</#list>
}
